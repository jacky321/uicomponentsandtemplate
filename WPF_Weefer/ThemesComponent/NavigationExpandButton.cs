﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.ThemesComponent
{
    [ContentProperty("Items")]
    public class NavigationExpandButton : Control, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        
        static NavigationExpandButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NavigationExpandButton), new FrameworkPropertyMetadata(typeof(NavigationExpandButton)));
           
        }

        //private bool _IsExpanded = false;
        //public bool IsExpanded
        //{
        //    get => _IsExpanded;
        //    set
        //    {
        //        if (value != _IsExpanded)
        //        {
        //            _IsExpanded = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}


        //public ObservableCollection<object> Items
        //{
        //    get { return (ObservableCollection<object>)GetValue(ItemsProperty); }
        //    set { SetValue(ItemsProperty, value); }
        //}

        //public static readonly DependencyProperty ItemsProperty =
        //    DependencyProperty.Register("Items", typeof(ObservableCollection<object>), typeof(NavigationExpandButton), null);

        //public object NavigationExpandButtonIcon
        //{
        //    get { return GetValue(NavigationExpandButtonIconProperty); }
        //    set { SetValue(NavigationExpandButtonIconProperty, value); }
        //}

        //public static readonly DependencyProperty NavigationExpandButtonIconProperty =
        //    DependencyProperty.Register("NavigationExpandButtonIcon", typeof(object), typeof(NavigationExpandButton), null);

        //public object NavigationExpandButtonText
        //{
        //    get { return GetValue(NavigationExpandButtonTextProperty); }
        //    set { SetValue(NavigationExpandButtonTextProperty, value); }
        //}

        //public static readonly DependencyProperty NavigationExpandButtonTextProperty =
        //    DependencyProperty.Register("NavigationExpandButtonText", typeof(object), typeof(NavigationExpandButton), null);

        //protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
