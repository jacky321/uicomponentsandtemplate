﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_Weefer.Models
{
    public class NavigationMenuClass
    {
        public NavigationMenuClass()
        {
           Children = new List<NavigationMenuClass>();
        }
        //ncgrp_ticketsystem
        public string MenuName { get; set; }
        public string IconText { get; set; }
        public bool NormalButton { get; set; }
        public bool RefreshExpand { get; set; }
        public List<NavigationMenuClass> Children { get; set; }
    }
}
