﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WPF_Weefer.Models
{
    public class DummyData
    {
        public string Dessert { get; set; }
        public string Calorie { get; set; }
        public string Fat { get; set; }
        public string Carb { get; set; }
        public string Protein { get; set; }
    }
}

 
