﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for ModalForgotPassword.xaml
    /// </summary>
    public partial class ModalForgotPassword : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler ForgotPasswordLoginClicked;
        public event EventHandler ForgotPasswordSendClicked;

        public ModalForgotPassword()
        {
            InitializeComponent();
            ForgotPasswordLoginClicked += ModalForgotPassword_ForgotPasswordLoginClicked;
            ForgotPasswordSendClicked += ModalForgotPassword_ForgotPasswordSendClicked;
        }

        private void ModalForgotPassword_ForgotPasswordSendClicked(object sender, EventArgs e)
        {
        }

        private void ModalForgotPassword_ForgotPasswordLoginClicked(object sender, EventArgs e)
        {
        }

        private void BtnForgotPasswordSend_Click(object sender, RoutedEventArgs e)
        {
            ForgotPasswordSendClicked?.Invoke(this, EventArgs.Empty);
        }

        private void BtnForgotPasswordLogin_Click(object sender, RoutedEventArgs e)
        {
            ForgotPasswordLoginClicked?.Invoke(this, EventArgs.Empty);
        }

        private string _ForgotPasswordEmail;
        public string ForgotPasswordEmail
        {
            get => _ForgotPasswordEmail;
            set
            {
                _ForgotPasswordEmail = value;
                NotifyPropertyChanged();
            }
        }
        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
