﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for ModalRegister.xaml
    /// </summary>
    public partial class ModalRegister : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler RegisterLoginClicked;
        public event EventHandler RegisterClicked;
        public event EventHandler RegisterTermAndConditionsClicked;

        public ModalRegister()
        {
            InitializeComponent();
            RegisterClicked += ModalRegister_RegisterClicked;
            RegisterLoginClicked += ModalRegister_RegisterLoginClicked;
            RegisterTermAndConditionsClicked += ModalRegister_RegisterTermAndConditionsClicked;
        }

        private void ModalRegister_RegisterTermAndConditionsClicked(object sender, EventArgs e)
        {
        }

        private void ModalRegister_RegisterLoginClicked(object sender, EventArgs e)
        {
        }

        private void ModalRegister_RegisterClicked(object sender, EventArgs e)
        {
        }

        private void BtnRegisterLogin_Click(object sender, RoutedEventArgs e)
        {
            RegisterLoginClicked?.Invoke(this, EventArgs.Empty);
        }

        private void BtnRegister_Click(object sender, RoutedEventArgs e)
        {
            RegisterClicked?.Invoke(this, EventArgs.Empty);
        }

        private void BtnTermAndConditions_Click(object sender, RoutedEventArgs e)
        {
            RegisterTermAndConditionsClicked?.Invoke(this, EventArgs.Empty);
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get => _IsChecked;
            set
            {
                if (value != _IsChecked)
                {
                    _IsChecked = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _RegisterFullName;
        public string RegisterFullName
        {
            get => _RegisterFullName;
            set
            {
                _RegisterFullName = value;
                NotifyPropertyChanged();
            }
        }

        private string _RegisterEmail;
        public string RegisterEmail
        {
            get => _RegisterEmail;
            set
            {
                _RegisterEmail = value;
                NotifyPropertyChanged();
            }
        }

        private string _RegisterPhone;
        public string RegisterPhone
        {
            get => _RegisterPhone;
            set
            {
                _RegisterPhone = value;
                NotifyPropertyChanged();
            }
        }

        private string _RegisterCompanyCode;
        public string RegisterCompanyCode
        {
            get => _RegisterCompanyCode;
            set
            {
                _RegisterCompanyCode = value;
                NotifyPropertyChanged();
            }
        }

        private string _RegisterCompanyName;
        public string RegisterCompanyName
        {
            get => _RegisterCompanyName;
            set
            {
                _RegisterCompanyName = value;
                NotifyPropertyChanged();
            }
        }

        private string _RegisterBaseCurrency;
        public string RegisterBaseCurrency
        {
            get => _RegisterBaseCurrency;
            set
            {
                _RegisterBaseCurrency = value;
                NotifyPropertyChanged();
            }
        }

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void CbIagreetotheTermAndConditions_Checked(object sender, RoutedEventArgs e)
        {
            IsChecked = true;
        }

        private void CbIagreetotheTermAndConditions_Unchecked(object sender, RoutedEventArgs e)
        {
            IsChecked = false;
        }
    }
}
