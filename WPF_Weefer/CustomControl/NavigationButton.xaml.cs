﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for NavigationButton.xaml
    /// </summary>
    public partial class NavigationButton : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public NavigationButton()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public object IconTextButton
        {
            get { return GetValue(IconTextButtonProperty); }
            set { SetValue(IconTextButtonProperty, value); }
        }

        public static readonly DependencyProperty IconTextButtonProperty =
            DependencyProperty.Register("IconTextButton", typeof(object), typeof(NavigationExpandButton), null);

        public object MiddleTextButton
        {
            get { return GetValue(MiddleTextButtonProperty); }
            set { SetValue(MiddleTextButtonProperty, value); }
        }

        public static readonly DependencyProperty MiddleTextButtonProperty =
            DependencyProperty.Register("MiddleTextButton", typeof(object), typeof(NavigationExpandButton), null);

        public ObservableCollection<object> DataContextButton
        {
            get { return (ObservableCollection<object>)GetValue(DataContextButtonProperty); }
            set { SetValue(DataContextButtonProperty, value); }
        }

        public static readonly DependencyProperty DataContextButtonProperty =
            DependencyProperty.Register("DataContextButton", typeof(ObservableCollection<object>), typeof(NavigationExpandButton), new UIPropertyMetadata(null));

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
