﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for Dialog.xaml
    /// </summary>
    public partial class Dialog : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
   
        public Dialog()
        {
            InitializeComponent();
            DataContext = this;
        }

        public object ModalContain
        {
            get { return GetValue(ModalContainProperty); }
            set { SetValue(ModalContainProperty, value); }
        }

        public static readonly DependencyProperty ModalContainProperty = 
            DependencyProperty.Register("ModalContain", typeof(object), typeof(Dialog), null);

        public object ModalFooter
        {
            get { return GetValue(ModalFooterProperty); }
            set { SetValue(ModalFooterProperty, value); }
        }

        public static readonly DependencyProperty ModalFooterProperty =
            DependencyProperty.Register("ModalFooter", typeof(object), typeof(Dialog), null);

        private string _ModalHeaderText;
        public string ModalHeaderText
        {
            get => _ModalHeaderText;
            set
            {
                _ModalHeaderText = value;
                NotifyPropertyChanged();
            }
        }

        private string _ModalBackgroundColor;
        public string ModalBackgroundColor
        {
            get => _ModalBackgroundColor;
            set
            {
                _ModalBackgroundColor = value;
                NotifyPropertyChanged();
            }
        }

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
