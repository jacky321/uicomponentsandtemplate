﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for NavigationExpandButton.xaml
    /// </summary>
    public partial class NavigationExpandButton : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public NavigationExpandButton()
        {
            InitializeComponent();
            this.DataContext = this;
            DataContextExpandButton = new ObservableCollection<object>();
        }

        private bool _RefreshExpand = false;
        public bool RefreshExpand
        {
            get => _RefreshExpand;
            set
            {
                if (value != _RefreshExpand)
                {
                    _RefreshExpand = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _IsExpanderButton;
        public bool IsExpanderButton
        {
            get => _IsExpanderButton;
            set
            {
                if (value != _IsExpanderButton)
                {
                    _IsExpanderButton = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public object IconTextExpandButton
        {
            get { return GetValue(IconTextExpandButtonProperty); }
            set { SetValue(IconTextExpandButtonProperty, value); }
        }

        public static readonly DependencyProperty IconTextExpandButtonProperty =
            DependencyProperty.Register("IconTextExpandButton", typeof(object), typeof(NavigationExpandButton), null);

        public object MiddleTextExpandButton
        {
            get { return GetValue(MiddleTextExpandButtonProperty); }
            set { SetValue(MiddleTextExpandButtonProperty, value); }
        }

        public static readonly DependencyProperty MiddleTextExpandButtonProperty =
            DependencyProperty.Register("MiddleTextExpandButton", typeof(object), typeof(NavigationExpandButton), null);

        public ObservableCollection<object> DataContextExpandButton
        {
            get { return (ObservableCollection<object>)GetValue(DataContextExpandButtonProperty); }
            set { SetValue(DataContextExpandButtonProperty, value); }
        }

        public static readonly DependencyProperty DataContextExpandButtonProperty =
            DependencyProperty.Register("DataContextExpandButton", typeof(ObservableCollection<object>), typeof(NavigationExpandButton), new UIPropertyMetadata(null));

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
