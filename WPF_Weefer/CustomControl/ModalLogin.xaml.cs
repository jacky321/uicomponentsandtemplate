﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.CustomControl
{
    /// <summary>
    /// Interaction logic for ModalLogin.xaml
    /// </summary>
    public partial class ModalLogin : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler LoginClicked;
        public event EventHandler LoginForgotPasswordClicked;
        public event EventHandler LoginregisterClicked;

        public ModalLogin()
        {
            InitializeComponent();
            LoginClicked += ModalLogin_loginClicked;
            LoginForgotPasswordClicked += ModalLogin_LoginForgotPasswordClicked;
            LoginregisterClicked += ModalLogin_LoginregisterClicked;
        }

        private void ModalLogin_LoginregisterClicked(object sender, EventArgs e)
        {
        }

        private void ModalLogin_LoginForgotPasswordClicked(object sender, EventArgs e)
        {
        }

        private void ModalLogin_loginClicked(object sender, EventArgs e)
        {
        }

        private void BtnLoginForgotPassword_Click(object sender, RoutedEventArgs e)
        {
            LoginForgotPasswordClicked?.Invoke(this, EventArgs.Empty);
        }

        private void BtnLoginRegister_Click(object sender, RoutedEventArgs e)
        {
            LoginregisterClicked?.Invoke(this, EventArgs.Empty);
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginClicked?.Invoke(this, EventArgs.Empty);
        }

        private string _LoginTenantName;
        public string LogintenantName
        {
            get => _LoginTenantName;
            set
            {
                _LoginTenantName = value;
                NotifyPropertyChanged();
            }
        }

        private string _LoginUserName;
        public string LoginUserName
        {
            get => _LoginUserName;
            set
            {
                _LoginUserName = value;
                NotifyPropertyChanged();
            }
        }

        private string _LoginPassword;
        public string LoginPassword
        {
            get => _LoginPassword;
            set
            {
                _LoginPassword = value;
                NotifyPropertyChanged();
            }
        }

        protected void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
