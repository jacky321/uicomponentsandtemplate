﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Weefer.Views
{
    /// <summary>
    /// Interaction logic for TextFieldPage.xaml
    /// </summary>
    public partial class TextFieldPage : Page
    {
        public MainWindow MainWindow { get; }
        public TextFieldPage(MainWindow mainWindow)
        {
            InitializeComponent();
            MainWindow = mainWindow;
        }
    }
}
