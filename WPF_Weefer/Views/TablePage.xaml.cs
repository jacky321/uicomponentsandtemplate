﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Weefer.Models;

namespace WPF_Weefer.Views
{
    /// <summary>
    /// Interaction logic for TablePage.xaml
    /// </summary>
    public partial class TablePage : Page
    {
        public MainWindow MainWindow { get; }
        public List<DummyData> listpreview { get; set; }
        public TablePage(MainWindow mainWindow)
        {
            InitializeComponent();
            MainWindow = mainWindow;
            Loaded += TablePage_Loaded;
        }

        private void TablePage_Loaded(object sender, RoutedEventArgs e)
        {
            var dummies = new List<DummyData>();

            dummies.Add(new DummyData { Dessert = "Frozen Yogurt", Calorie = "159", Fat = "6", Carb = "24", Protein = "4" });
            dummies.Add(new DummyData { Dessert = "Frozen Yogurt", Calorie = "159", Fat = "6", Carb = "24", Protein = "4" });
            dummies.Add(new DummyData { Dessert = "Frozen Yogurt", Calorie = "159", Fat = "6", Carb = "24", Protein = "4" });
            dummies.Add(new DummyData { Dessert = "Frozen Yogurt", Calorie = "159", Fat = "6", Carb = "24", Protein = "4" });

            ListTableDummyData.ItemsSource = dummies;
        }
    }
}
