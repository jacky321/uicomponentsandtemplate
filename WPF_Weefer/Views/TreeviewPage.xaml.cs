﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Weefer.CustomControl;
using WPF_Weefer.Models;

namespace WPF_Weefer.Views
{
    /// <summary>
    /// Interaction logic for TreeviewPage.xaml
    /// </summary>
    public partial class TreeviewPage : Page
    {
        public List<NavigationMenuClass> navigationMenuClasses;
        public TreeviewPage()
        {
            InitializeComponent();
            Loaded += TreeviewPage_Loaded;
        }

        private void TreeviewPage_Loaded(object sender, RoutedEventArgs e)
        {
            var DataBase = new List<NavigationMenuClass>()
            {
               new NavigationMenuClass()
               {
                   MenuName = "TextLevel1.2",
                   IconText = "Icon1.2",
                   Pressable = false,
                   Children =
                   {
                       new NavigationMenuClass()
                       {
                           MenuName = "TextLevel2.1",
                           IconText = "Icon2.1",
                           Pressable = false,
                           Children = 
                           {
                                new NavigationMenuClass()
                                {
                                    MenuName = "TextLevel3.1",
                                    IconText = "Icon3.1",
                                    Pressable = true,
                                    Children =
                                    {
                                         new NavigationMenuClass()
                                         {
                                             MenuName = "TextLevel4.1",
                                             IconText = "Icon4.1",
                                             Pressable = false,
                                             Children = null
                                         },

                                         new NavigationMenuClass()
                                         {
                                             MenuName = "TextLevel4.2",
                                             IconText = "Icon4.2",
                                             Pressable = false,
                                             Children = null
                                         },

                                         new NavigationMenuClass()
                                         {
                                             MenuName = "TextLevel4.3",
                                             IconText = "Icon4.3",
                                             Pressable = false,
                                             Children = null
                                         }
                                    }
                                }
                           }
                       },

                       new NavigationMenuClass()
                       {
                           MenuName = "TextLevel2.2",
                           IconText = "Icon2.2",
                           Pressable = false,
                           Children = null
                       }
                   }
               },
            };
            AddToMenuButtonContainIfHaveChild(ref MenuButtonContain, DataBase);
        }

        private void AddToMenuButtonContainIfHaveChild(ref StackPanel menuButtonContain, List<NavigationMenuClass> Listitems)
        {
            //if (Listitems != null)
            //{
            //    foreach (var item in Listitems)
            //    {
            //        var ButtonCustom = new NavigationExpandButton();
            //        ButtonCustom.IconNavigationButton = item.IconButton;
            //        ButtonCustom.ContentNavigationButton = item.MenuName;
            //        ButtonCustom.Background = Brushes.Honeydew;
                    
            //        menuButtonContain.Children.Add(ButtonCustom);

            //        AddToMenuButtonContainIfHaveChild(ref menuButtonContain, item.Children);
            //    }
            //}
        }
        private void ButtonCustom_ButtonClick(object sender, EventArgs e)
        {
            //var buttonClicked = sender as NavigationMenuButton;
            //buttonClicked.ChildFrame.ItemsSource = buttonClicked.Childrens;
            //buttonClicked.ChildFrame.Visibility = true;
        }

    }
}
