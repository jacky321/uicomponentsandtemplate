﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_Weefer.CustomControl;

namespace WPF_Weefer.Views
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Page
    {
        public MainWindow MainWindow { get; }
        public LoginPage(MainWindow mainWindow)
        {
            InitializeComponent();
            MainWindow = mainWindow;
        }

        private void ModalLogin_LoginClicked(object sender, EventArgs e)
        {
            MainWindow.Content = new TreeviewPage();
        }

        private void ModalLogin_LoginForgotPasswordClicked(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(((Action)(() =>
            {
                ModalForgotPassword.Visibility = Visibility.Visible;
                ModalLogin.Visibility = Visibility.Hidden;
                ModalRegister.Visibility = Visibility.Hidden;
            })));
        }

        private void ModalLogin_LoginregisterClicked(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(((Action)(() =>
            {
                ModalForgotPassword.Visibility = Visibility.Hidden;
                ModalLogin.Visibility = Visibility.Hidden;
                ModalRegister.Visibility = Visibility.Visible;
            })));
        }

        private void ModalForgotPassword_ForgotPasswordLoginClicked(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(((Action)(() =>
            {
                ModalForgotPassword.Visibility = Visibility.Hidden;
                ModalLogin.Visibility = Visibility.Visible;
                ModalRegister.Visibility = Visibility.Hidden;
            })));
        }

        private void ModalForgotPassword_ForgotPasswordSendClicked(object sender, EventArgs e)
        {
        }

        private void ModalRegister_RegisterClicked(object sender, EventArgs e)
        {
        }

        private void ModalRegister_RegisterLoginClicked(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke(((Action)(() =>
            {
                ModalForgotPassword.Visibility = Visibility.Hidden;
                ModalLogin.Visibility = Visibility.Visible;
                ModalRegister.Visibility = Visibility.Hidden;
            })));
        }
    }
}
